<?php

namespace App\Controller;

use App\Entity\Announce;
use App\Form\AnnounceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/announces", name="admin_list_announce")
     */
    public function list()
    {
        $announces = $this
            ->getDoctrine()
            ->getRepository(Announce::class)
            ->findAllAnnounce();

        return $this->render('admin/list.html.twig', [
            'site_title' => 'Leboncoin',
            'announces' => $announces
        ]);
    }

    /**
     * @Route("/admin/announce/create", name="admin_create_announce")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        // on instancie un nouvel objet Announce
        $announce = new Announce();

        // on créer le formulaire
        $form = $this->createForm(AnnounceType::class, $announce);
        $form->handleRequest($request);

        // si le formulaire est soumit et si il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($announce);
            $em->flush();

            // on envoi un petit message flash de confirmation
            $this->addFlash('success', 'L\'annonce '. $announce->getTitle().' a été crée avec succès');

            // on redirige l'utilisateur sur la liste des announces
            return $this->redirectToRoute('admin_list_announce');
        }

        return $this->render('admin/new.html.twig', [
            'site_title' => 'Leboncoin',
            'announce' => $announce,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/announce/{id}", name="admin_edit_announce", methods="GET|POST")
     * @param Announce $announce
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, Announce $announce = null)
    {
        // on créer le formulaire
        $form = $this->createForm(AnnounceType::class, $announce);
        $form->handleRequest($request);

        // si le formulaire est soumit et si il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($announce);
            $em->flush();

            // on envoi un petit message flash de confirmation
            $this->addFlash('success', 'L\'annonce '. $announce->getTitle().' a été édité avec succès');

            // on redirige l'utilisateur sur la liste des announces
            return $this->redirectToRoute('admin_list_announce');
        }

        return $this->render('admin/edit.html.twig', [
            'site_title' => 'Leboncoin',
            'announce' => $announce,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/announce/{id}", name="admin_delete_announce", methods="DELETE")
     * @param Announce $announce
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Announce $announce, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $announce->getId(), $request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($announce);
            $em->flush();

            // on envoi un petit message flash de confirmation
            $this->addFlash('success', 'L\'annonce '. $announce->getTitle().' a été supprimé avec succès');

            return $this->redirectToRoute('admin_list_announce');
        }

    }
}
