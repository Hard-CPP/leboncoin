<?php

namespace App\Controller;

use App\Entity\Announce;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        // on récupère nos announces depuis notre repository
        $announces = $this
            ->getDoctrine()
            ->getRepository(Announce::class)
            ->findFeaturedAnnounces();

        return $this->render('home/index.html.twig', [
            'site_title' => 'Leboncoin',
            'announces' => $announces
        ]);
    }

    /**
     * @Route("/announces", name="page_announces")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function announces(PaginatorInterface $paginator, Request $request) : Response
    {
        $announces = $paginator
            ->paginate($this
                    ->getDoctrine()
                    ->getRepository(Announce::class)
                    ->findAllAnnounces(), $request->query->getInt('page', 1));

        $featured = $this
            ->getDoctrine()
            ->getRepository(Announce::class)
            ->findFeaturedAnnounces(7);

        return $this->render('home/announces.html.twig', [
            'site_title' => 'Leboncoin',
            'announces' => $announces,
            'featured' => $featured
        ]);
    }
}
