<?php

namespace App\DataFixtures;

use App\Entity\Announce;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AnnounceFixtures extends Fixture
{
    /**
     * Génération de 50 annonces avec une génération aléatoire des données grâce à la librairie Faker
     * composer require fzaninotto/faker | https://github.com/fzaninotto/faker
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // on déclare vouloir utiliser la librairie Faker
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 50; $i++) {

            $announce = new Announce();
            $announce
                ->setTitle($faker->words(4, true))
                ->setDescription($faker->words(15, true))
                ->setDescriptionLong($faker->paragraphs(3, true))
                ->setPrice($faker->numberBetween(10, 5000))
                ->setSold(false)
                ->setFeatured($faker->boolean(50));

            $manager->persist($announce);
        }

        $manager->flush();
    }
}
