<?php

namespace App\Repository;

use App\Entity\Announce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Announce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Announce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Announce[]    findAll()
 * @method Announce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnounceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Announce::class);
    }

    /**
     * Sélection aléatoire des annonces mises en avant triées par date de création
     * @param int $max
     * @return mixed
     */
    public function findFeaturedAnnounces($max = 3)
    {
        return $this->createQueryBuilder('p')
            ->Where('p.featured = true')
            ->orderBy('p.createAt', 'desc')
            ->orderBy('rand()')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

    /**
     * Récupération de toutes les annonces disponible non vendu
     * @return Query
     */
    public function findAllAnnounces() : Query
    {
        return $this->createQueryBuilder('p')
            ->Where('p.sold = false')
            ->andWhere('p.featured = false')
            ->orderBy('p.createAt', 'desc')
            ->getQuery();
    }
}
